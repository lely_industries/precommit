#!/usr/bin/env bash
# shellcheck shell=bash
# INFO:   This script mimics the behavior of the build-server (build + run docker image) for local usage.
#         This way it is possible to test the docker image + execution with the same result as on the build-server.
# USAGE:  Call the script (in)directly with required argument (see output with --help)
#
# REQUIRES  : (see --help)
# OPTIONAL  :
# EXPOSES   :
###################################################################################################################
set -Eeuo pipefail

# Parse parameters
# See `man getopt'. https://gist.github.com/bobpaul/ecd74cdf7681516703f20726431eaceb
SHORT="a::d::f::e::hp:r::i:v::l::w::"
LONG="arg::,no-build,build-arg::,no-cache,docker-dir::,docker-file::,entrypoint::,executable::,"
LONG+="help,image-dir::,image-path:,image-registry::,image-url:,image-variant::,image-version::"
LONG+=",local-dir::,name::,no-run,no-secret,userns::,workdir::"
USAGE="Builds and runs the projects container adjusted by the options and commands provided.
The defaults should follow the values required to mimic the buildimage template.

Usage: $(basename -- "$0") [OPTIONS] [--] [COMMANDS]
Options:
  -a, --arg             RUN_ARGS[*]     =${RUN_ARGS[*]}
      --no-build        NO_BUILD        =${NO_BUILD-}
      --build-arg       BUILD_ARGS[*]   =${BUILD_ARGS[*]}
      --no-cache        NO_CACHE        =${NO_CACHE-}
  -d, --docker-dir      DOCKER_DIR      =${DOCKER_DIR-}
  -f, --docker-file     DOCKER_FILE     =${DOCKER_FILE-}
  -e, --entrypoint      ENTRYPOINT      =${ENTRYPOINT-}
      --executable      Use docker or the provided executable instead of podman
                        EXECUTABLE      =${EXECUTABLE-}
  -h, --help            Prints this help text and exits.
      --image-dir       IMAGE_DIR       =${IMAGE_DIR-}
  -p, --image-path      IMAGE_PATH      =${IMAGE_PATH-}
  -r, --image-registry  IMAGE_REGISTRY  =${IMAGE_REGISTRY-}
  -i, --image-url       IMAGE_URL       =${IMAGE_URL-}
      --image-variant   IMAGE_VARIANT   =${IMAGE_VARIANT-}
  -v, --image-version   IMAGE_VERSION   =${IMAGE_VERSION-}
  -l, --local-dir       LOCAL_DIR       =${LOCAL_DIR-}
      --name            CONTAINER_NAME  =${CONTAINER_NAME-}
      --no-run          NO_RUN          =${NO_RUN-}
      --no-secret       Removes setting '.netrc' as a secret
                        NO_SECRET       =${NO_SECRET-}
      --userns          USERNS          =${USERNS-}
  -w, --workdir         WORKDIR         =${WORKDIR-}
Default command:        COMMAND         =${COMMAND-}

Builds and runs the projects container adjusted by the options and commands provided.
The defaults should follow the values required to mimic the buildimage template.
"

! (getopt --test >/dev/null || [[ $? -ne 4 ]]) || ! printf 'getopt is outdated.\n'
PARSED=$(getopt --options "${SHORT}" --longoptions "${LONG}" --name "$0" -- "$@")
eval set -- "${PARSED}"
if [[ -n ${RUN_ARGS[*]-x} ]]; then
    : "${RUN_ARGS:=}"
else
    unset RUN_ARGS
fi
if [[ -n ${BUILD_ARGS[*]-x} ]]; then
    : "${BUILD_ARGS:=}"
else
    unset BUILD_ARGS
fi
while [[ $# -gt 0 ]]; do
    case "$1" in
    -a | --arg)
        shift
        if [[ -z $1 ]]; then
            unset RUN_ARGS
        else
            RUN_ARGS+=("--$1")
        fi
        ;;
    --no-build)
        NO_BUILD=1
        ;;
    --build-arg)
        shift
        if [[ -z $1 ]]; then
            unset BUILD_ARGS
        else
            BUILD_ARGS+=("--build-arg=$1")
        fi
        ;;
    --no-cache)
        NO_CACHE=1
        ;;
    -d | --docker-dir)
        shift
        DOCKER_DIR="$1"
        ;;
    -f | --docker-file)
        shift
        DOCKER_FILE="$1"
        ;;
    -e | --entrypoint)
        shift
        ENTRYPOINT="$1"
        ;;
    --executable)
        shift
        EXECUTABLE="$1"
        : "${EXECUTABLE:=docker}"
        ;;
    -h | --help)
        SHOW_HELP=1
        ;;
    --image-dir)
        shift
        IMAGE_DIR="$1"
        ;;
    -p | --image-path)
        shift
        IMAGE_PATH="$1"
        ;;
    -r | --image-registry)
        shift
        IMAGE_REGISTRY="$1"
        ;;
    -i | --image-url)
        shift
        IMAGE_URL="$1"
        ;;
    --image-variant)
        shift
        IMAGE_VARIANT="$1"
        ;;
    -v | --image-version)
        shift
        IMAGE_VERSION="$1"
        ;;
    -l | --local-dir)
        shift
        LOCAL_DIR="$1"
        ;;
    --name)
        shift
        CONTAINER_NAME="$1"
        ;;
    --no-run)
        NO_RUN=1
        ;;
    -s | --no-secret)
        NO_SECRET=1
        ;;
    --userns)
        shift
        USERNS="$1"
        ;;
    -w | --workdir)
        shift
        WORKDIR="$1"
        ;;
    --)
        shift
        break
        ;;
    *)
        printf 'Error: Cannot parse: %s\n' "${*@Q}"
        exit 1
        ;;
    esac
    shift
done

# Defaults
WPWD="$(pwd -W 2>/dev/null || pwd || true)"
PODMAN="podman"
ARTIFACTORY_DOMAIN="artifactory.lelyonline.com"
: "${EXECUTABLE:="${PODMAN}"}"
[[ ${EXECUTABLE} == "${PODMAN}" ]] && : "${USERNS="keep-id"}"
if [[ ${EXECUTABLE} == "${PODMAN}" || $(uname -s || true) != *_NT* ]]; then
    WINDOWS_MODE=false
else
    WINDOWS_MODE=true
fi
unset CACHE
: "${DOCKER_FILE="${BASH_SOURCE:+"${BASH_SOURCE/"$(basename -- "${BASH_SOURCE[0]}")"/"Dockerfile"}"}"}"
[[ -v NO_CACHE ]] && CACHE=("--no-cache")
[[ -z ${DOCKER_FILE} || ${EXECUTABLE} != "${PODMAN}" ]] && NO_CACHE=1
[[ -z ${DOCKER_FILE} ]] && NO_BUILD=1
DOCKER_FILE_DIR="$(dirname -- "${DOCKER_FILE:-"${BASH_SOURCE-}"}")"
: "${DOCKER_DIR="$(git -C "${DOCKER_FILE_DIR}" rev-parse --show-toplevel 2>/dev/null || true)"}"
if git diff --quiet; then
    CI_COMMIT_SHA="$(git -C "${DOCKER_FILE_DIR}" rev-list --max-count=1 HEAD -- 2>/dev/null || true)"
else
    CI_COMMIT_SHA="local"
fi
if [[ -z ${IMAGE_URL-} ]]; then
    : "${IMAGE_REGISTRY="${ARTIFACTORY_DOMAIN}"}"
    IMAGE_REGISTRY="${IMAGE_REGISTRY##*://}"
    if [[ -z ${IMAGE_PATH-} ]]; then
        IMAGE_PATH="$(git -C "${DOCKER_FILE_DIR}" ls-remote --get-url 2>/dev/null || true)"
        if [[ -n ${IMAGE_PATH} ]]; then
            # remove .git extension to get path.
            IMAGE_PATH="${IMAGE_PATH%.git}"
            if [[ ${IMAGE_PATH} == git@* ]]; then
                # Filter on 'ssh' paths in git.
                IMAGE_PATH="${IMAGE_PATH#git@*:}"
            else
                # Strip scheme from URL. (Scheme cannot contain '/'.)
                if [[ ${IMAGE_PATH%%:*} != *[[:punct:]]* ]]; then
                    IMAGE_PATH="${IMAGE_PATH#*:}"
                fi
                # Strip the netloc from the URL. (Netloc must start with '//'.)
                if [[ ${IMAGE_PATH#//} == */* ]]; then
                    IMAGE_PATH="${IMAGE_PATH#//*/}"
                else
                    IMAGE_PATH="${IMAGE_PATH#//*}"
                fi
            fi
            if [[ ${IMAGE_REGISTRY} == "${ARTIFACTORY_DOMAIN}" ]]; then
                IMAGE_PATH="${IMAGE_PATH%%/*}-docker/${IMAGE_PATH}"
            fi
            IMAGE_PATH="${IMAGE_REGISTRY}/${IMAGE_PATH}"
            : "${PROJECT="$(basename -- "${IMAGE_PATH}")"}"
            # Multi image builds
            if [[ -n ${DOCKER_FILE} && -n ${DOCKER_DIR} && ! -v IMAGE_DIR ]]; then
                IMAGE_DIR="/$(realpath "--relative-to=$(cd "${DOCKER_DIR}" && pwd)" -- "${DOCKER_FILE_DIR}" 2>/dev/null || true)"
                IMAGE_DIR_REMOVAL="${IMAGE_DIR##*/docker/}"
                if [[ ${IMAGE_DIR_REMOVAL} == "${IMAGE_DIR}" ]]; then
                    unset IMAGE_DIR
                else
                    IMAGE_DIR="${IMAGE_DIR_REMOVAL}"
                fi
            fi
            IMAGE_PATH+="${IMAGE_DIR:+"/${IMAGE_DIR}"}${IMAGE_VARIANT-}"
            # Caching
            if [[ ! -v NO_CACHE ]]; then
                : "${DEFAULT_BRANCH="main"}"
                : "${COMMIT_REF_NAME="$(git -C "${DOCKER_FILE_DIR}" branch --show-current 2>/dev/null || true)"}"
                if [[ ${DEFAULT_BRANCH} != "${COMMIT_REF_NAME}" ]]; then
                    CACHE+=("--cache-from=${IMAGE_PATH}/cache/${DEFAULT_BRANCH}")
                fi
                if [[ -n ${COMMIT_REF_NAME} ]]; then
                    CACHE+=("--cache-from=${IMAGE_PATH}/cache/${COMMIT_REF_NAME}")
                fi
            fi
        else
            IMAGE_PATH="${DOCKER_FILE_DIR#*:}"
        fi
    fi
    IMAGE_PATH="${IMAGE_PATH%/}"
    IMAGE_PATH="${IMAGE_PATH#/}"
    # Multi stage builds chain by CI_COMMIT_SHA build argument.
    : "${IMAGE_VERSION="${CI_COMMIT_SHA}"}"
    IMAGE_URL="${IMAGE_PATH:+"${IMAGE_PATH}${IMAGE_VERSION:+":${IMAGE_VERSION}"}"}"
else
    IMAGE_URL="${IMAGE_URL##*://}"
    if [[ ${IMAGE_URL%:*} == "${IMAGE_URL}" || ${IMAGE_URL#"${IMAGE_URL%:*}":} == */* ]]; then
        IMAGE_PATH="${IMAGE_URL%/}"
        IMAGE_VERSION=""
    else
        IMAGE_PATH="${IMAGE_URL%:*}"
        IMAGE_VERSION="${IMAGE_URL#"${IMAGE_PATH}:"}"
    fi
    IMAGE_REGISTRY="${IMAGE_PATH%%/*}"
    IMAGE_PATH="${IMAGE_PATH#/}"
fi
: "${PROJECT="$(basename -- "$(pwd || true)")"}"
PROJECT="${PROJECT%%/}"
: "${CONTAINER_NAME="${PROJECT}"}"
if ${WINDOWS_MODE}; then
    : "${LOCAL_DIR="$(cmd.exe //C CD || true)"}"
    : "${WORKDIR="c:\\mnt\\${PROJECT:-"workdir"}"}"
else
    : "${LOCAL_DIR="$(pwd || true)"}"
    : "${WORKDIR="//mnt/${PROJECT:-"workdir"}"}"
fi
[[ $# -eq 0 ]] && eval set -- "${COMMAND-}"
DOCKER_DIR="$(realpath "--relative-to=${WPWD}" -- "${DOCKER_DIR}" 2>/dev/null || true)"

if ! ${WINDOWS_MODE} && [[ ! -v NO_SECRET && -r ${HOME}/.netrc ]]; then
    BUILD_SECRETS=("--secret=id=.netrc,src=${HOME}/.netrc")
    RUN_SECRETS=("--mount=type=bind,source=${HOME}/.netrc,target=/run/secrets/.netrc,ro=true")
else
    unset BUILD_SECRETS RUN_SECRETS
fi

if [[ -z ${RUN_ARGS-x} ]]; then
    RUN_ARGS=("--interactive" "--rm" "--tty" "${RUN_ARGS[@]:1}")
fi
if [[ -z ${BUILD_ARGS-x} ]]; then
    ARGS=("${BUILD_ARGS[@]:1}")
    BUILD_ARGS=()
    if [[ -n ${CI_COMMIT_SHA} && ! -v IMAGE_VERSION && -f ${DOCKER_FILE} ]] &&
        grep --fixed-strings --quiet -- "CI_COMMIT_SHA" "${DOCKER_FILE}"; then
        BUILD_ARGS+=("--build-arg=CI_COMMIT_SHA=${CI_COMMIT_SHA}")
    fi
    BUILD_ARGS+=("${ARGS[@]}")
fi
[[ -z ${RUN_ARGS-x} ]] && unset RUN_ARGS
[[ -z ${BUILD_ARGS-x} ]] && unset BUILD_ARGS

# Feedback
[[ -n ${SHOW_HELP-} ]] && printf '%s\n' "${USAGE}"
printf '%s%s%s\n' "${PROJECT:+"${PROJECT}: "}" "${DOCKER_DIR:+"${DOCKER_DIR} "}" "${DOCKER_FILE}"
printf 'Image: %s\n' "${IMAGE_URL-}"
if [[ ! -v NO_RUN ]]; then
    [[ -n ${LOCAL_DIR-} && -n ${WORKDIR-} ]] && printf 'mount %s -> %s\n' "${WORKDIR}" "${LOCAL_DIR}"
    [[ -n ${ENTRYPOINT-}${*} ]] &&
        printf 'Command:%s %s\n' "Command:${ENTRYPOINT+" \'--entrypoint=${ENTRYPOINT}\'"}" "${*@Q}"
fi
if [[ ! -v NO_BUILD && ! -v NO_CACHE && -n ${CACHE-} ]]; then
    printf 'Caching: \n'
    printf '\t%s\n' "${CACHE[@]}"
fi

: "${IMAGE_URL:?"An image is required."}"

# Log in if possible and required
if [[ -r ${HOME}/.netrc && ! -v SHOW_HELP ]] &&
    ([[ ! -v NO_BUILD ]] || ! "${EXECUTABLE}" image exists "${IMAGE_URL}") &&
    ([[ ${EXECUTABLE} != "${PODMAN}" ]] || ! "${EXECUTABLE}" login --get-login "${IMAGE_REGISTRY}" >/dev/null 2>&1); then
    #
    PYTHON="$(command -v python3 || command -v python || command -v py || true)"
    if [[ -n ${PYTHON} ]]; then
        read_netrc() {
            "${PYTHON}" -c "import netrc;item=netrc.netrc().authenticators('${IMAGE_REGISTRY}');item and print(item[$1])" || true
        }
        USERNAME="$(
            set -e
            read_netrc 0
        )"
        if [[ -n ${USERNAME} ]]; then
            read_netrc 2 | (
                "${EXECUTABLE}" login --password-stdin --username="${USERNAME}" "${IMAGE_REGISTRY}" || true
            )
        fi
    fi
fi

# Build
if [[ ! -v NO_BUILD ]]; then
    (
        set -x
        ${SHOW_HELP+: }"${EXECUTABLE}" build \
            "${BUILD_ARGS[@]}" \
            "${CACHE[@]}" \
            "--file=${DOCKER_FILE}" \
            "${BUILD_SECRETS[@]}" \
            "--tag=${IMAGE_URL}" \
            ${DOCKER_DIR:+"${DOCKER_DIR}"}
    )
fi

# Run
if [[ ! -v NO_RUN ]]; then
    (
        set -x
        ${SHOW_HELP+: }"${EXECUTABLE}" run \
            ${ENTRYPOINT+"--entrypoint=${ENTRYPOINT}"} \
            ${LOCAL_DIR:+${WORKDIR:+"--mount=type=bind,source=${LOCAL_DIR},target=${WORKDIR}"}} \
            ${LOCAL_DIR:+${WORKDIR:+"--workdir=${WORKDIR}"}} \
            ${CONTAINER_NAME:+"--name=${CONTAINER_NAME}"} \
            "${RUN_SECRETS[@]}" \
            ${USERNS:+"--userns=${USERNS}"} \
            "${RUN_ARGS[@]}" \
            "${IMAGE_URL}" \
            "$@"
    )
fi
