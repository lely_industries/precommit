# -- FILE: features/example.feature
# Lorum ipsum
@all
@test.sanity_test
Feature: Showing off behave

  @build
  @fixture.start_test
  Scenario: Run a simple test
    Given we have behave installed
     When we implement 5 tests
     Then behave will test them for us!
      And this is also executed
