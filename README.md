# [Pre-commit]

Implementation of <https://pre-commit.com>

## Why

- Uniformity: The code looks the same everywhere. This makes reviewing easier.\
  Additionally, the style is corrected before review so you do not have to review for it.
- Preventing merge conflicts: Merge conflicts are bad.
- Includes a few lightweight sanity checks.

## A [pre-commit] change in main caused my merge/rebase to go bad

The following procedure prevents merge conflicts or a bad merge/rebase on main. Use it when a
[pre-commit] update introduces a lot of changes preventing a smooth merge or rebase. The procedure
creates three commits on your branch. If you don't like that, you can always squash afterwards.

- Preparation: Make sure your working git clone is committed, pushed and cleaned. This allows you to
  [go back to this point] in case of issues.
- [Locally update pre-commit](#updating-a-submodule-configuration) and commit using
  `git commit -n -am "update pre-commit"`.
- Run `pre-commit run --all` and commit normally.
- Execute the merge: `git merge origin main`, commit normally and push if you are happy with the
  result.

Remember that you can [exclude commits in blame]. Git visualization tools support this as well.

## [pre-commit] Installation

### [pre-commit] application and tools

Note that these tools are already installed when you followed the instructions fom
<https://gitlab.lelyonline.com/common/cm/wiki/-/blob/main/Windows10.md> or succesfully ran
`lely-bootstrap.cmd` from these instructions.

- Install the tool, preferably for all users as administrator or root using [Python]3:

    ```shell
    pip install -U pre-commit
    ```

- The tools [shfmt] and [terraform] require manual installation:

    - To install [shfmt] under Windows:
        - Download `shftm_*_windows_amd64.exe` from <https://github.com/mvdan/sh/releases>.
        - Rename the downloaded file to `shfmt.exe`.
        - Save it to `C:\Program Files\shfmt`.
        - Add the directory to your `PATH` variable.
    - To install [shfmt] under Linux, either:

        ```shell
        sudo snap install shfmt
        ```

        or:

        - Download `shftm_*_linux_amd64` from <https://github.com/mvdan/sh/releases>.
        - Rename the downloaded file to `shfmt`.
        - Save it to `/usr/local/bin`. You could try something like: (See docker/shfmt_version for
          the actual version of shfmt to download.)

        ```shell
        sudo curl -Lo /usr/local/bin/shfmt https://github.com/mvdan/sh/releases/download/v3.4.2/shfmt_v3.4.2_linux_amd64
        sudo chmod +x /usr/local/bin/shfmt
        ```

    - To install [terraform] under Windows:

        ```shell
        choco install --yes terraform
        ```

    - To install [terraform] under Linux:\
      <https://www.terraform.io/docs/cli/install/apt.html>

    - To properly make [pre-commit] work, [MSYS2] `bash.exe` must be earlier in the path then [WSL]
      `bash.exe`. To start [WSL], type `wsl` instead of `bash`.

        After installing [git], [WSL] and optionally [MSYS2]. Use `where bash` in a command prompt
        (`CMD`) to find out the current order. The [git] bash (or the [MSYS2] bash) must be before
        the [WSL] `C:\Windows\System32\bash.exe`.

        ```shell
        >where bash
        C:\Program Files\Git\usr\bin\bash.exe
        C:\Windows\System32\bash.exe
        ```

        I recommend just removing the legacy Windows `bash.exe`:

        ```shell
        DEL /F /Q "%SystemRoot%\system32\bash.exe"
        ```

### [pre-commit] activation per repository

For each repository:

- Activate the tool in your git clone directory:

    ```shell
    pre-commit install
    ```

    After activation, every `git commit` will use [pre-commit] to check the file that are about to
    be committed.

- Run [pre-commit] stand alone on all your files:

    ```shell
    pre-commit run --all-files
    ```

You can also run a specific tool or `hook` or run one or all `hooks` on a specific set of files.\
Use the `--no-verify` or `-n` flag with a `git commit` to skip running [pre-commit].\
Use `pre-commit uninstall` to deactivate [pre-commit] for a repository.

## [pre-commit] repository configuration

Note that the configuration is committed to Git. Your project probably already has configured
[pre-commit]. If so, you are ready to use [pre-commit].

The [pre-commit] tool uses the tools configured in the `.pre-commit-config.yaml` configuration file.
When [pre-commit] is installed and activated, it runs these tools when you execute a `git commit` on
the files that were changed. These tools usually have configuration files as well.

### Installation as a test that is part of the build

Requires the [configuration files].

- Include the template in your `.gitlab-ci.yml` file:

    ```yml
    include:
        - project: $CM_TEMPLATES
          ref: tag-template
          file:
              - templates/precommit.gitlab-ci.yml

    variables:
        PRECOMMIT_VERSION: tag-pre-commit
    ```

- The predefined `$CM_TEMPLATES` variable points to the template project.
- Set the ref (`tag-template` in the example) to the version of the template project you want use.
- Set the ref (`tag-pre-commit` in the example) to the version of the pre-commit project you want
  use.
- If you use [configuration by submodule], the submodule version shall be the same as
  `tag-pre-commit`. The pre-commit job that runs on the build node will fail if the submodule
  version does not match that of the template _and it will tell you how to correct it_ (just above
  the pre-commit output, including the correct version.)

### Installation of the configuration files

- Copy the configuration files from the template directory of this project to the root of your
  project. [Static configuration]\
  **or**
- Submodule this project and link the configuration templates . [Submodule configuration]

### Static configuration

Copy the configuration files from the template directory to the root of your repo:

```shell
    find ../precommit/templates -type f -not -name ".gitlab-ci.yml" -exec cp -ft. {} \+
```

(Assuming you have a clone of the pre-commit project.)

Remember to do this each time you want to update the [pre-commit] configuration to a more recent
version.

### Submodule configuration

Instead of copying the [configuration files], submodule this repository and create soft links to the
[configuration files]. I recommend this.

- Create the submodule:

    ```shell
    git submodule add ../../../cm/precommit.git ext/precommit
    git -C  ext/precommit checkout tag-pre-commit
    ```

    Note that the amount of `../` before `cm/precommit` depends on the location of your project in
    [Gitlab].

- Create the soft links (forced):

    ```shell
    find ext/precommit/templates -type f -not -name ".gitlab-ci.yml" -exec ln -sft. {} \+
    ```

- Commit changes:

    ```shell
    git add .
    git commit -anm "Adding precommit."
    ```

### Updating a [Submodule configuration]

- Update the pre-commit submodule:

    ```shell
    git -C  ext/precommit checkout tag-pre-commit
    ```

    The submodule version (`tag-pre-commit` in the example) shall be the same as set in the template
    file `templates/precommit.gitlab-ci.yml` which you included.

    Note that the pre-commit job that runs on the build node will fail if the submodule version does
    not match that of the template _and it will tell you how to correct it_ (just above the
    pre-commit output, including the correct version.)

- Make sure that the soft links are still working.

    ```shell
    > ls -al
    ...
    .clang-format -> templates/.clang-format
    .editorconfig -> templates/.editorconfig
    .pre-commit-config.yaml -> templates/.pre-commit-config.yaml
    .prettierrc.toml -> templates/.prettierrc.toml
    .reformat-gherkin.yaml -> ext/precommit/templates/.reformat-gherkin.yaml
    pyproject.toml -> templates/pyproject.toml
    ...
    ```

- Force update to fix the links if they are broken or incomplete:

    ```shell
    find ext/precommit/templates -type f -not -name ".gitlab-ci.yml" -exec ln -sft. {} \+
    ```

- Commit your changes.

    ```shell
    git commit -anm "Updating precommit."
    ```

- Run [pre-commit] with the updated configuration so check for new style changes.

    ```shell
    pre-commit run --all-files
    ```

- Commit your changes. The style changes are now in a separate commit as the configuration changes.

    ```shell
    git commit -am "Apply updated precommit."
    ```

Note that the pre-commit test job shows the versions of the tools that you yourself need to update
manually on your computer.

### Run the pre-commit image locally

You can run the pre-commit docker image locally, so that you get the same results as with the test
job.

```shell
./ext/precommit/docker/docker.sh --no-build
```

(By using `--no-build`, the script will not try to build the image, it will pull the pre-build one
or fail.)

## Start working with [pre-commit] on your [Gitlab] project

- Configure and install [pre-commit] as described above.

### Clean the main branch

- Create a merge request and clone the branch to add the [pre-commit] configuration and fix errors.
- Add the configuration files or submodule `../CM/precommit.git`.
- Also add the [pre-commit] test job, but add `allow_failure: true` to it.
- Run [pre-commit] stand alone.\
  Repeat until it finishes without making changes.
- **Do not** make any manual changes.\
  **Do not** commit the changes that [pre-commit] makes in the same branch where you add the
  configuration and solve any errors found.
- Integrate the merge request with the [pre-commit] configuration and error fixes.
- Create a merge request and clone the branch to apply [pre-commit].
- Run [pre-commit] stand alone.\
  Repeat until it finishes without making changes.
- **Do not** make any manual changes.
- Integrate the merge request with the changes that [pre-commit] applied.
- Create a merge request to remove `allow_failure: true` from your [pre-commit] test job and
  integrate it.

It is important that the merge request that applies [pre-commit] your code does not make any other
changes. And because you cannot activate the [pre-commit] test job until after that, you need three
integrations to perform adding [pre-commit] to your repository.

Remember that you can [exclude commits in blame]. Git visualization tools support this as well.

### Prevent subsequent merge conflicts in your feature branches

- Start using [pre-commit] in your branch when you are ready for it, just as with a regular merge or
  rebase.
- If you wish to rebase, first rebase until the commit before the [pre-commit] commit.
- Run [pre-commit] stand alone.\
  Repeat until it finishes without making changes.
- **Do not** make any manual changes
- If running [pre-commit] fails because of errors, solve those errors with a separate commit before
  continuing. You may need to merge with or rebase to the commit on the master with the [pre-commit]
  configuration and error fixes.
- Activate [pre-commit] on your repository.

## Additional [static program analysis] for [CodeClimate] integrated in [Gitlab]

If you use the [code quality template] provided by [Gitlab] then the file
`templates/.codeclimate.yml` enables extra plugins for more checks. These can be enabled for your
project by either copying or soft linking this file as well.

It does not enable [pylint] because of an issue. Use the [pylint template] provided by your
configuration management team as follows:

`.gitlab-ci.yml`:\

```yml
include:
    - project: $CM_TEMPLATES
      ref: 1.1.1
      file:
          - templates/PyLint.gitlab-ci.yml
    - template: Code-Quality.gitlab-ci.yml
```

## Code style example

[Eastern Polish Christmas Tree Notation]

```java
public
DataPair[] getHotelInformation(String hotelId, String informationId)
                                      {
                                        return getHotelInfo("EN", hotelId, informationId);
                                      }

public
DataPair[] getHotelInformation(String lang, String hotelId, String informationId)
                                      {

                           String key = "_HOINF_"+lng+"_"+hotelId+"_"+informationId;
                       DataPair[] tbl = (DataPair[])csh.getObject(key);
                         if(tbl!=null)  return tbl;

                        Connection cn = null;
           OracleCallableStatement cs = null;
                                  try {
                           String qry = " begin HotelServices.getHotelInfo(?, ?, ?, ?, ?); end; ";
                               logger . debug("---"+qry+" "+hotelId+" "+informationId);
                                   cn = DriverManager.getConnection("jdbc:weblogic:pool:oraclePool",null);
                                   cs = (OracleCallableStatement)cn.prepareCall(qry);
                                   cs . registerOutParameter(1,java.sql.Types.INTEGER);
                                   cs . registerOutParameter(2,java.sql.Types.OTHER);
                                   cs . setString(3,hotelId);
                                   cs . setString(4,informationId);
                                   cs . setString(5,lang);
                                   cs . execute();
                              int sta = cs.getInt(1);
                            if(sta!=0)  throw new Exception("status not zero sta="+sta);
                         ResultSet rs = cs.getResultSet(2);
                                  tbl = getDataPairArray(rs);
                               logger . debug("sta="+sta+" key="+key+" cn="+cn);
                                  csh . put(key,tbl);
                                      }
                                 catch(Exception e)
                                      {
                               logger . debug("!!! "+e.toString()+" "+key);
                                      }
                               finally
                                      {
                                  try {
                      if(cs!=null) cs . close();
                      if(cn!=null) cn . close();
                                      }
                                 catch(Exception x)
                                      {
                               logger . debug("!!! "+x.toString()+" "+key);
                               logger . error("!!! "+x.toString());
                                      }
                                      }
                                return tbl;
                                      }
```

## Additional code quality checkers

This project also stores the configuration of some code quality tools, so that these do not report
unsolvable false positives.

[Codeclimate] configuration file: `.codeclimate.yml`.

The [codeclimate] tool contains a collection of code quality tools and runs by adding the
`Code-Quality.gitlab-ci.yml` template to your `.gitlab-ci.yml` file.

The default submodule directory (`ext`) is excluded from the checks. And the code quality tools
below are added. Only [cppcheck] is enabled by default.

### [cppcheck]

This tool seems to behave normally and requires no additional configuration.

```shell
cppcheck .
```

### [markdownlint]

Configuration files: `.mdlrc`, `.markdownlint.yml`.

This tool checks your markdown files and can fix some of the issues it finds automatically.

The plugin for [codeclimate] refuses to use the configuration files, and as such always reports a
severe amount of false positives. It is disabled because of this.

The command line tool just works.

```shell
markdownlint --fix .
```

### [pylint]

The [pylint] configuration is in the `[tool.pylint]` section of `pyproject.toml` and can be
overridden by using a `.pylintrc` file. Note that the ancient version of [pylint] shipped with
Ubuntu 20.04 LTS does not support `pyproject.toml` as a config file.

The [codeclimate] plugin does not accept parameters. This prevents selecting the correct directories
containing one or more modules or tests. And it just shows the help output. And even if it worked,
according to the documentation some checks had to be disabled because of the missing dependencies.

Use the [pylint template] instead, preferably with your own container that is set for testing and as
such has all the dependencies. Add [pylint] to these dependencies.

### [shellcheck]

The [shellcheck] plugin from [codeclimate] most likely uses an old version that still gives false
positives with advice that will introduce bugs. This is a pity because the latest version does not
suffer from this defect, and the advice on how to solve the issues is excellent. And shell script is
a tricky language used in critical parts of software.

Running [shellcheck] on the command line recursively for a project is tricky.

```shell
git grep -El "^#!(.*/|.*env +)(sh|bash|ksh)" | xargs -r shellcheck -axo all
```

## Conventional commits

If you want to enforce [conventional commit](https://www.conventionalcommits.org/en/v1.0.0/) message
formatting, run the following command on all developers' machines:

```plain
pre-commit install --hook-type commit-msg
```

---

[code quality template]: https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html
[codeclimate]: https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html
[configuration files]: #installation-of-the-configuration-files
[cppcheck]: https://cppcheck.sourceforge.io/
[eastern polish christmas tree notation]:
    https://web.archive.org/web/20150531212725/http:/www.kuro5hin.org/story/2004/6/1/43942/41236
[exclude commits in blame]:
    https://git-scm.com/docs/git-blame#Documentation/git-blame.txt---ignore-revs-fileltfilegt
[git]: https://git-scm.com/
[gitlab]: https://gitlab.lelyonline.com
[go back to this point]:
    https://stackoverflow.com/questions/4114095/how-do-i-revert-a-git-repository-to-a-previous-commit
[markdownlint]: https://github.com/igorshubovych/markdownlint-cli
[msys2]: https://www.msys2.org
[pre-commit]: https://pre-commit.com
[pylint template]: https://gitlab.lelyonline.com/common/cm/pylint
[pylint]: https://github.com/PyCQA/pylint/
[python]: https://www.python.org
[shellcheck]: https://github.com/koalaman/shellcheck
[shfmt]: https://github.com/mvdan/sh/releases
[static configuration]: #static-configuration
[static program analysis]: https://en.wikipedia.org/wiki/Static_program_analysis
[submodule configuration]: #submodule-configuration
[terraform]: https://www.terraform.io
[wsl]: https://docs.microsoft.com/en-us/windows/wsl/about
