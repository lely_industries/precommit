# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog], and this project adheres to [Semantic Versioning].

## [Unreleased]

Fixed:

- Fixed the root path bug in pylint by bumping the version to the fixed one.

## [3.5.0]

Fixed:

- Fixed isort settings. The current one was a workaround for an old bug which now prevents correctly
  sorting First-party libraries (e.g. local modules).

## [3.4.3] - 2022-10-17

Fixed:

- Workaround for a bug in Gitlab and or Terraform\
  <https://gitlab.com/gitlab-org/gitlab/-/issues/367980>

## [3.4.2] - 2022-10-11

Added:

- Added support for Artifactory

## [3.3.3] - 2022-04-15

Fixed:

- CLang Format not formatting c/c++ files.

---

[unreleased]: https://gitlab.lelyonline.com/common/cm/precommit "main"
[3.5.0]: https://gitlab.lelyonline.com/common/cm/precommit/-/tags/3.5.0
[3.4.3]: https://gitlab.lelyonline.com/common/cm/precommit/-/tags/3.4.3
[3.4.2]: https://gitlab.lelyonline.com/common/cm/precommit/-/tags/3.4.2
[3.3.3]: https://gitlab.lelyonline.com/common/cm/precommit/-/tags/3.3.3
[keep a changelog]: https://keepachangelog.com/en/1.0.0/
[semantic versioning]: https://semver.org/spec/v2.0.0.html
