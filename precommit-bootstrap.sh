#!/usr/bin/env bash
set -Eeuo pipefail

#
# Constants
#
declare -i TRUE=0
declare -i FALSE=1
GITLAB_DOMAIN="gitlab.lelyonline.com"
PRECOMMIT_REPO="common/cm/precommit.git"
PRECOMMIT_BRANCH="main"

#
# Set up some initial variables
#
# If DEBUG=$TRUE (i.e. 0), then debug message are printed.
DEBUG=${FALSE}
# If FORCE_SYMLINKS=$TRUE (i.e. 0), then symlinks will be recreated.
FORCE_SYMLINKS=${FALSE}
# Show no messages
QUIET=${FALSE}
# Don't execute any changes by default (i.e. 'dry-run')
# You must use -e|--execute for this script to do anything
EXECUTE=${FALSE}
# Will be 0 if everything is installed and configured correctly, otherwise we report with a 2.
NO_ISSUES=0
# An array of log messages to print out at the end.
LOG_MESSAGES=()

USAGE_MESSAGE="Usage: $(basename -- "$0") [OPTIONS]
Options:
  -h, --help              Prints this help text and exits
  -d, --debug             Enable debugging
  -q, --quiet             Don't show any messages
  -e, --execute           Execute changes (the default is a 'dry-run')
  -f, --force-symlinks    Force recreating the symlinks
  -d, --update            Updates this script to the latest version from GitLab

Exit codes:
- 0: your repository is correctly configured
- 1: there was an error running this script
- 2: your repository is not correctly configured
"

debug() {
    #
    # Prints a debug message.
    #
    local DRY_RUN_MSG=""
    if [[ ${EXECUTE} -eq ${FALSE} ]]; then
        DRY_RUN_MSG=" (dry-run)"
    fi

    if [[ ${QUIET} -ne ${TRUE} && ${DEBUG} -eq ${TRUE} ]]; then
        if [[ $# -eq 1 ]]; then
            printf '[DEBUG]%s: %s\n' "${DRY_RUN_MSG}" "$1"
        else
            # The function name was passed as the 2nd argument
            printf '[DEBUG]%s %s(): %s\n' "${DRY_RUN_MSG}" "$2" "$1"
        fi
    fi
}

error() {
    #
    # Prints an error message.
    #
    if [[ $# -eq 1 ]]; then
        printf '[ERROR]: %s\n' "$1" >&2
    else
        # The function name was passed as the 2nd argument
        printf '[ERROR] %s(): %s\n' "$2" "$1" >&2
    fi
}

print_usage() {
    #
    # Print usage information.
    #
    printf '%s\n' "${USAGE_MESSAGE}"
}

parse_arguments() {
    #
    # Parse command-line arguments.
    #
    local SHORT="hdeqfu"
    local LONG="help,debug,execute,quiet,force-symlinks,update"
    ! (getopt --test >/dev/null || [[ $? -ne 4 ]]) || ! printf 'getopt is outdated.\n'
    PARSED=$(getopt --options "${SHORT}" --longoptions "${LONG}" --name "$0" -- "$@")
    eval set -- "${PARSED}"

    while [[ $# -gt 0 ]]; do
        case "$1" in
        # Print the usage message
        -h | --help)
            print_usage
            exit 0
            ;;
        # Enable debugging messages
        -d | --debug)
            shift
            DEBUG=${TRUE}
            ;;
        -e | --execute)
            shift
            EXECUTE=${TRUE}
            ;;
        -q | --quiet)
            shift
            QUIET=${TRUE}
            ;;
        -f | --force-symlinks)
            shift
            FORCE_SYMLINKS=${TRUE}
            ;;
        -u | --update)
            shift
            update_script
            exit 0
            ;;
        --)
            shift
            break
            ;;
        # Handle illegal parameters
        *)
            printf 'Error: Cannot parse: %s\n' "${*@Q}"
            print_usage
            exit 1
            ;;
        esac
    done

    validate_argument_combinations
}

validate_argument_combinations() {
    #
    # Check if there are any 'illegal' combinations of arguments.
    #
    if [[ ${QUIET} -eq ${TRUE} && ${DEBUG} -eq ${TRUE} ]]; then
        error "Cannot specify -q|--quiet and -d|--debug together"
        exit 1
    fi

    if [[ ${EXECUTE} -eq ${FALSE} && ${FORCE_SYMLINKS} -eq ${TRUE} ]]; then
        error "Cannot specify -f|--force without also specifying -e|--execute"
        exit 1
    fi
}

mark_issue_found() {
    #
    # Make a note that there were issues using a magic number 2.
    #
    NO_ISSUES=2
}

update_script() {
    #
    # Downloads the latest version from GitLab.
    #
    debug "update_script() - Downloading ssh://git@${GITLAB_DOMAIN}/${PRECOMMIT_REPO}" "${FUNCNAME[@]:0:1}"
    git archive --format tar \
        --remote="ssh://git@${GITLAB_DOMAIN}/${PRECOMMIT_REPO}" \
        "${PRECOMMIT_BRANCH}" \
        "precommit-bootstrap.sh" |
        tar --extract --to-stdout >|"$0"
    chmod +x "$0"
    append_to_log_messages "The script has been updated to the latest version"
}

get_to_correct_directory() {
    #
    # Figure out the base directory of the script.
    #
    cd -- "$(git rev-parse --show-superproject-working-tree || true)" || return
    cd -- "$(git rev-parse --show-toplevel || true)" || return
    debug "Running in $(pwd || true)" "${FUNCNAME[@]:0:1}"
}

check_for_precommit_executable() {
    #
    # Check if the pre-commit executable is installed.
    #
    local VERSION
    if VERSION=$(pre-commit --version 2>/dev/null); then
        append_to_log_messages "${VERSION} is installed"
    else
        if [[ ${EXECUTE} -eq ${TRUE} ]]; then
            error "pre-commit - Please install the pre-commit executable"
            exit 1
        else
            append_to_log_messages "Missing - the pre-commit binary must be installed"
            mark_issue_found
        fi
    fi
}

ensure_precommit_hook() {
    #
    # Installs it if it is not installed.
    #
    if [[ ${EXECUTE} -eq ${TRUE} ]]; then
        local OUTPUT
        OUTPUT=$(pre-commit install | tail -n1)
        append_to_log_messages "${OUTPUT}"
    else
        if grep --fixed-strings --quiet -- "https://pre-commit.com" ".git/hooks/pre-commit"; then
            append_to_log_messages "The pre-commit hook is installed"
        else
            append_to_log_messages "Missing - the pre-commit hook must be installed"
            mark_issue_found
        fi
    fi
}

ensure_precommit_submodule() {
    #
    # Ensures that the precommit submodule is installed.
    #
    local MODULE_STATUS
    local PRE_COMMIT_SUBMODULE_ADDED="${TRUE}"
    MODULE_STATUS=$(git submodule | grep --fixed-string -- "ext/precommit" || printf 'not added\n')
    if [[ ${MODULE_STATUS} == "not added" ]]; then
        PRE_COMMIT_SUBMODULE_ADDED="${FALSE}"
    fi

    # If the submodule has not been added, add it, then re-run the above checks
    # so that it can be initialized.
    if [[ ${PRE_COMMIT_SUBMODULE_ADDED} -ne ${TRUE} ]]; then
        # There is no ext/precommit directory and the module has not been added

        local ORIGIN
        local DEPTH
        local GIT_SUBMODULE_COMMAND
        # Get the https: or git: origin of the url
        ORIGIN="$(git remote get-url origin)"
        # Convert the two possible git origin urls from:
        # - git@gitlab.lelyonline.com:a/b/c/project.git
        # - https://gitlab.lelyonline.com/a/b/c/project.git
        # to:
        # /a/b/c/project.git
        ORIGIN="${ORIGIN/*gitlab.lelyonline.com?//}"
        # Count how many times "/" appears in the string
        DEPTH="$(printf '%s\n' "${ORIGIN}" | tr -cd "/" | wc -c)"
        # Build up the command to something like:
        # # git submodule add ../../../../commmon/cm/precommit.git ext/precommit
        GIT_SUBMODULE_COMMAND+="git submodule add "
        for ((i = 0; i < DEPTH; i++)); do
            GIT_SUBMODULE_COMMAND+="../"
        done
        GIT_SUBMODULE_COMMAND+="common/cm/precommit.git ext/precommit"

        if [[ ${EXECUTE} -eq ${TRUE} ]]; then
            debug "pre-commit - Executing ${GIT_SUBMODULE_COMMAND}" "${FUNCNAME[@]:0:1}"
            # Execute the command to create the submodule
            ${GIT_SUBMODULE_COMMAND} >/dev/null
            append_to_log_messages "Submodule ext/precommit added"
        else
            append_to_log_messages "Missing - the ext/precommit submodule must be added"
            append_to_log_messages "Missing - the ext/precommit submodule must be initialized"
            mark_issue_found
        fi

        MODULE_STATUS="$(git submodule | grep --fixed-string -- "ext/precommit" || printf 'not added\n')"
        if [[ ${MODULE_STATUS} == "not added" ]]; then
            PRE_COMMIT_SUBMODULE_ADDED="${FALSE}"
        else
            PRE_COMMIT_SUBMODULE_ADDED="${TRUE}"
        fi
    fi

    # Check if the submodule has been initialized
    if [[ ${PRE_COMMIT_SUBMODULE_ADDED} -eq ${TRUE} ]]; then
        # If the line starts with a '-', then the submodule is
        # configured but not initialized.
        if [[ ${MODULE_STATUS} == -* ]]; then
            if [[ ${EXECUTE} -eq ${TRUE} ]]; then
                append_to_log_messages "Submodule ext/precommit is added, but not initialized. Initializing..." "${FUNCNAME[@]:0:1}"
                git submodule update --init --recursive >/dev/null 2>&1
                append_to_log_messages "Submodule ext/precommit initialized"
            else
                append_to_log_messages "Missing - the ext/precommit submodule must be initialized"
                mark_issue_found
            fi
        else
            append_to_log_messages "The submodule is added and initialized."
        fi
    else
        if [[ ${EXECUTE} -eq ${TRUE} ]]; then
            error "pre-commit - The submodule could not be added"
            exit 1
        fi
    fi
}

ensure_precommit_symlinks() {
    #
    # Ensure all the necessary precommit symlinks are present.
    #
    local LOCAL_PATH
    if [[ ${EXECUTE} -eq ${TRUE} ]]; then
        while read -r fn; do
            ln -sft. "${fn}"
            LOCAL_PATH="$(basename "${fn}")"
            debug "Added symlink ${LOCAL_PATH} > ${fn}" "${FUNCNAME[@]:0:1}"
        done <<<"$(find ext/precommit/templates/ -type f -not -name .gitlab-ci.yml || true)"
    else
        local MISSING_LINK=${FALSE}
        if [[ -d ext/precommit/templates ]]; then
            while read -r fn; do
                LOCAL_PATH="$(basename "${fn}")"
                if [[ ! -e ${LOCAL_PATH} ]]; then
                    MISSING_LINK=${TRUE}
                    debug "Symlink ${LOCAL_PATH} > ${fn} is missing" "${FUNCNAME[@]:0:1}"
                fi
            done <<<"$(find ext/precommit/templates/ -type f -not -name .gitlab-ci.yml || true)"
        else
            MISSING_LINK="${TRUE}"
        fi

        if [[ ${MISSING_LINK} -eq ${TRUE} ]]; then
            append_to_log_messages "Missing - Not all symlinks to ext/precommit exist"
            mark_issue_found
        else
            append_to_log_messages "All symlinks ext/precommit exist"
        fi
    fi
}

check_terraform_executable() {
    #
    # Check if the terraform executable is installed.
    #
    local VERSION
    if VERSION=$(terraform --version 2>/dev/null | head -n1); then
        append_to_log_messages "${VERSION} is installed"
    else
        append_to_log_messages "Missing - Terraform is NOT installed"
        mark_issue_found
    fi
}

check_shfmt_executable() {
    #
    # Check if the shfmt executable is installed.
    #
    local VERSION
    if VERSION=$(shfmt --version 2>/dev/null | head -n1); then
        append_to_log_messages "shfmt ${VERSION} is installed"
    else
        append_to_log_messages "Missing - shfmt is NOT installed"
        mark_issue_found
    fi
}

append_to_log_messages() {
    #
    # Append the given message to the final log messages.
    #
    LOG_MESSAGES+=("$1")
}

print_log_messages() {
    if [[ ${QUIET} -eq ${TRUE} ]]; then
        return
    fi

    if [[ ${#LOG_MESSAGES[@]} -gt 0 ]]; then
        for ((i = 0; i < ${#LOG_MESSAGES[@]}; i++)); do
            printf '[LOG]: %s\n' "${LOG_MESSAGES[${i}]}"
        done
    fi
}

main() {
    #
    # The 'main' function.
    #

    pushd . &>/dev/null

    # Parse the command-line arguments.
    parse_arguments "${@}"

    get_to_correct_directory

    check_for_precommit_executable
    ensure_precommit_hook
    ensure_precommit_submodule
    ensure_precommit_symlinks

    check_terraform_executable
    check_shfmt_executable

    if [[ ${DEBUG} -eq ${TRUE} && ${QUIET} -ne ${TRUE} ]]; then
        printf '\n'
    fi
    print_log_messages

    popd &>/dev/null
}

main "${@}"
exit "${NO_ISSUES}"
